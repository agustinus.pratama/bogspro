# Bogspro
This application is an attempt to create GPS Providers API wrappers.

## Getting Started

Add the following line to your Gemfile:
```ruby
gem 'bogspro', :git => 'https://gitlab.com/agustinus.pratama/bogspro.git', :branch => 'master'
```

Then run `bundle install`

[TBD]

## Supported GPS Providers

[TBD]

## Contributing
Currently its only for Bocistudio developer.

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
