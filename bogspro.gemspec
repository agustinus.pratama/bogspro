$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "bogspro/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "bogspro"
  s.version     = Bogspro::VERSION
  s.platform    = Gem::Platform::RUBY
  s.licenses    = ["MIT"]
  s.summary     = "Bocistudio GPS Providers."
  s.email       = "info@bocistudio.com"
  s.homepage    = "https://www.bocistudio.com"
  s.description = "Bocistudio attempt to handle GPS Providers without third party."
  s.authors     = ["Agustinus AP"]

  s.files         = Dir["{app,config,lib}/**/*", "MIT-LICENSE", "CHANGELOG.md", "README.md"]
  s.require_paths = ["lib"]
  s.required_ruby_version = '> 2.5.0'


  s.add_dependency "railties", "> 5.0.0"

  s.add_development_dependency "sqlite3"
end
